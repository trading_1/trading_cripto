#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 14:27:25 2021

@author: sergio
"""
import time
import utils_trading.utils as ut
import utils_trading.log_proyect as log
from datetime import datetime, timezone

#######################################
def busca_valles(conf_client,market,params):
    
    if market == 'EUR-BTC':
        market = 'BTC-EUR';    
    
    cartera_posible = dict()
    
    #Se calcula el precio actual
    newData = conf_client.client.get_product_ticker(product_id=market)
    media_ult=float(newData['price'])
    
    #Contra el precio de hace 15 minutos
    media_penult = ut.calcula_medias_abs(conf_client,market,15)
    
    media_dia = ut.calcula_medias_abs(conf_client,market,360)
    
    diff_current_ult = 0
    porcent = 0
    
    try:
        porcent = (media_ult - media_penult) / media_penult * 100
        porcent_media_dia = (media_ult - media_dia) / media_dia * 100
    except:
        print(media_ult)
        print(media_penult)        
    
    try:
        newData = conf_client.client.get_product_ticker(product_id=market)
        currentPrice=float(newData['price'])
        #Esto dice si esta subiendo respecto 15m atras
        diff_current_ult = (currentPrice - media_ult) / media_ult * 100        
    except:
       print(newData)
    
    
    
    if market != 'BTC-EUR':

        if porcent <= -0.4 and diff_current_ult >= 0.01 and diff_current_ult <= 0.4 and porcent_media_dia >= -0.5:
            log.completo('Si considera valle para market: '+ market)
            cartera_posible [market] = (media_dia - currentPrice) / currentPrice * 100
    else:
        
        log.completo('Busca valle en market: '+ market)

        if not(porcent <= 0):
            log.completo('No considera valle porque -porcent- {} es mayor que 0'.format(porcent))
            
        if not(diff_current_ult >= 0):
            log.completo('No considera valle porque -diff_current_ult- {} es menor que 0'.format(diff_current_ult))

        #Si ha pasado mas de dos horas reinicio el precio mercado, ya que es posible que no baje
        startdate = params.precio_ultima_venta_fecha.strftime("%Y-%m-%dT%H:%M") 
        enddate = datetime.now(tz=timezone.utc).strftime("%Y-%m-%dT%H:%M")
        diff_hour = (datetime.strptime(enddate, "%Y-%m-%dT%H:%M") - datetime.strptime(startdate, "%Y-%m-%dT%H:%M"))
        horas_diff = diff_hour.total_seconds() / 3600
        
        if currentPrice <= params.precio_ultima_venta_mercado * 0.99 or horas_diff >= 2:            
            log.completo('Si considera valle porque -currentPrice- {} es menor que ultima venta {} o han pasado {} horas'.format(currentPrice,params.precio_ultima_venta_mercado,horas_diff))
        else:
            log.completo('No considera valle porque -currentPrice- {} es mayor que ultima venta {} o han pasado {} horas'.format(currentPrice,params.precio_ultima_venta_mercado,horas_diff))

        if porcent <= 0.4 and diff_current_ult >= 0 and diff_current_ult <= 0.5 and (currentPrice <= params.precio_ultima_venta_mercado * 0.99 or horas_diff >= 2):
            log.completo('Si considera valle para market: '+ market)
            cartera_posible [market] = (media_dia - currentPrice) / currentPrice * 100
            
        
    return cartera_posible 
        
    
#####################################    
    #Inicializo el dict
def calcula_moneda_inver(conf_client,cartera,params):

    if cartera.moneda_actual == 'BTC':
        #Si estamos en BTC. Se ponen todas las monedas menos BTC y EUR
        monedas={'BAND', 'BCH', 'BNT', 'EOS', 'ETC', 'ETH', 'LINK'}
        #monedas borradas UMA
    else:
        #Si estamos en XXX o EUR. Se pone BTC
        monedas={'BTC'}

    market_destino = dict()
    
    contador = 1
    market_final = None
    bool_caso_tercera_moneda = False
    
    for x in monedas:
        mercado_formado = ut.compone_mercado(x,cartera.moneda_actual)
        market_destino_recibido=busca_valles(conf_client, mercado_formado,params)
        
        if mercado_formado == 'BTC-EUR' and market_destino_recibido and cartera.moneda_actual not in ('BTC','EUR'):
            market_final = cartera.moneda_actual + '-BTC'
            bool_caso_tercera_moneda = True
        
        if market_destino_recibido:
            market_destino.update(market_destino_recibido)
        if contador == 2:
            time.sleep(2)
            contador=0
        
        contador = contador + 1
    
        
    #ordeno la market_destino y selecciono el ultimo elemento    
    if market_destino:
        market_destino=sorted(market_destino.items(),key=lambda x:x[1])
        if bool_caso_tercera_moneda == False:
            market_final = market_destino[-1][0]
               
    return market_final
    