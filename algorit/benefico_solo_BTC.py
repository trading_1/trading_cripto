#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script para analizar y tomar decisiones de compra/venta de BTC según tendencias y condiciones de mercado.

"""

import algorit.calcula_inversion_solo_BTC as ci
import utils_trading.log_proyect as log


def calcula_ganancia_minima(conf_client, cartera, params):
    """
    Calcula si se ha recuperado la inversión mínima basada en la comisión.

    Args:
        conf_client: Configuración del cliente.
        cartera: Objeto con información del monedero actual.
        params: Parámetros configurados para la inversión.

    Returns:
        bool: True si se ha alcanzado la ganancia mínima, False en caso contrario.
    """
    bool_recupera_ganancia = False
    result = 0

    if params.precio_ultima_compra != 0:
        result = params.precio_ultima_compra * conf_client.comision

    if params.precio_ultima_compra == 0 or cartera.conversion_euros_actual >= result:
        bool_recupera_ganancia = True

    log.completo(f"Ganancia mínima necesaria: {result}")
    return bool_recupera_ganancia


def supera_min_perdida(cartera, params):
    """
    Verifica si se ha superado el precio mínimo de pérdida.

    Args:
        cartera: Objeto con información del monedero actual.
        params: Parámetros configurados para la inversión.

    Returns:
        bool: True si se ha superado el mínimo de pérdida, False en caso contrario.
    """
    if cartera.moneda_actual != 'EUR' and cartera.conversion_euros_actual < params.min_precio_perdida:
        params.valoracion_inversion = 'Perdida'
        return True
    return False


def analista_mercado(conf_client, cartera, params, be_market):
    """
    Analiza el mercado y determina si comprar o vender.

    Args:
        conf_client: Configuración del cliente.
        cartera: Objeto con información del monedero actual.
        params: Parámetros configurados para la inversión.
        be_market: Objeto que analiza tendencias del mercado.

    Returns:
        bool: True si se debe comprar o vender, False en caso contrario.
    """
    visto_bueno = False
    analista = be_market.analista_mercado()

    bool_ganancia_min = calcula_ganancia_minima(conf_client, cartera, params)
    bool_supera_min_perdida = supera_min_perdida(cartera, params)

    # Decisión de compra
    if (
        cartera.moneda_actual == 'EUR'
        and analista.compra_por_tendencia_corta
        and analista.compra_precio_limite
        and not analista.mercado_a_la_baja
        and not analista.mercado_cerca_del_hist
        and analista.compra_rsi_1h
    ):
        visto_bueno = True

    log.completo(f"Compra por tendencia corta: {analista.compra_por_tendencia_corta}")
    log.completo(f"Compra por precio límite: {analista.compra_precio_limite}")
    log.completo(f"Mercado a la baja: {analista.mercado_a_la_baja}")
    log.completo(f"Mercado cerca del máximo histórico: {analista.mercado_cerca_del_hist}")

    # Decisión de venta
    if (
        cartera.moneda_actual != 'EUR'
        and analista.venta_por_tendencia_corta
        #and analista.venta_rsi_1h
        and bool_ganancia_min
    ) or bool_supera_min_perdida:
        visto_bueno = True

    log.completo(f"Venta por tendencia corta: {analista.venta_por_tendencia_corta}")
    log.completo(f"Ganancia mínima alcanzada: {bool_ganancia_min}")
    log.completo(f"Se ha perdido demasiado: {bool_supera_min_perdida}")

    return visto_bueno


def accion_realizar(conf_client, cartera, params, be_market):
    """
    Determina la acción a realizar en función del análisis del mercado.

    Args:
        conf_client: Configuración del cliente.
        cartera: Objeto con información del monedero actual.
        params: Parámetros configurados para la inversión.
        be_market: Objeto que analiza tendencias del mercado.

    Returns:
        str: Acción recomendada ('compra_desde_eur', 'venta_a_euros' o 'espera').
    """
    accion = 'espera'
    analista_res = analista_mercado(conf_client, cartera, params, be_market)

    log.completo(f"Estabilidad del mercado {cartera.market_actual}: {be_market.tendencia_24h}")
    log.completo(f"Mercado {cartera.market_actual} en descenso: {be_market.mercado_a_la_baja}")

    if cartera.moneda_actual == 'EUR' and analista_res:
        log.completo("Acción: Compra desde euros.")
        accion = 'compra_desde_eur'
    elif cartera.moneda_actual != 'EUR' and analista_res:
        log.completo("Acción: Venta a euros.")
        accion = 'venta_a_euros'

    return accion
