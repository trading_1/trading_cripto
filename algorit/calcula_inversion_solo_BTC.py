#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 14:27:25 2021

@author: sergio
"""
import time
import utils_trading.utils as ut
import utils_trading.log_proyect as log
from datetime import datetime, timezone

#######################################
def parametros_compra_prefil_alto(porcent,diff_current_ult,currentPrice,params,horas_diff,precio_max):
    
    if (porcent <= -0.15 or currentPrice <= params.precio_ultima_venta_mercado * 0.995 ):
        b_valle = True 
        log.completo('Si considera valle porque -porcent- {} es menor que -0.15'.format(porcent))
        log.completo('o -currentPrice- {} es menor que ultima venta {} menos el 2%'.format(currentPrice,params.precio_ultima_venta_mercado * 0.98))
    else:
        b_valle = False
        log.completo('No considera valle porque -porcent- {} es mayor que -0.15'.format(porcent))
        log.completo('o -currentPrice- {} es mayor que ultima venta {} menos el 2%'.format(currentPrice,params.precio_ultima_venta_mercado * 0.98))
    
    if diff_current_ult >= -0.03 and diff_current_ult <= 0.5:
        b_situ_act = True 
        log.completo('Si considera valle porque -diff_current_ult- {} es mayor que 0 o mayor que 0.5'.format(diff_current_ult))
    else:
        b_situ_act = False
        log.completo('No considera valle porque -diff_current_ult- {} es menor que 0 o mayor que 0.5'.format(diff_current_ult))
    
    if (currentPrice <= params.precio_ultima_venta_mercado * 0.995 or horas_diff >= 2):
        b_ult_comp = True 
        log.completo('Si considera valle porque -currentPrice- {} es menor que ultima venta {} o han pasado {} horas mas de 2 horas'.format(currentPrice,params.precio_ultima_venta_mercado,horas_diff))
    else:
        b_ult_comp = False
        log.completo('No considera valle porque -currentPrice- {} es mayor que ultima venta {} o han pasado {} horas'.format(currentPrice,params.precio_ultima_venta_mercado,horas_diff))
                
    if b_valle and b_situ_act and b_ult_comp:
        compra = True
    else:        
        compra = False
        

    return compra

def parametros_compra_prefil_bajo(porcent,diff_current_ult,currentPrice,params,horas_diff,precio_max):
    
    if (porcent <= -0.4 or currentPrice <= params.precio_ultima_venta_mercado * 0.97 ):
        b_valle = True 
        log.completo('Si considera valle porque -porcent- {} es menor que -0.15'.format(porcent))
        log.completo('o -currentPrice- {} es menor que ultima venta {} menos el 2%'.format(currentPrice,params.precio_ultima_venta_mercado * 0.98))
    else:
        b_valle = False
        log.completo('No considera valle porque -porcent- {} es mayor que -0.15'.format(porcent))
        log.completo('o -currentPrice- {} es mayor que ultima venta {} menos el 2%'.format(currentPrice,params.precio_ultima_venta_mercado * 0.98))
    
    if diff_current_ult >= -0.03 and diff_current_ult <= 0.5:
        b_situ_act = True 
        log.completo('Si considera valle porque -diff_current_ult- {} es mayor que 0 o mayor que 0.5'.format(diff_current_ult))
    else:
        b_situ_act = False
        log.completo('No considera valle porque -diff_current_ult- {} es menor que 0 o mayor que 0.5'.format(diff_current_ult))
    
    if (currentPrice <= params.precio_ultima_venta_mercado * 0.97 or horas_diff >= 8):
        b_ult_comp = True 
        log.completo('Si considera valle porque -currentPrice- {} es menor que ultima venta {} o han pasado {} horas mas de 2 horas'.format(currentPrice,params.precio_ultima_venta_mercado,horas_diff))
    else:
        b_ult_comp = False
        log.completo('No considera valle porque -currentPrice- {} es mayor que ultima venta {} o han pasado {} horas'.format(currentPrice,params.precio_ultima_venta_mercado,horas_diff))
        
    if b_valle and b_situ_act and b_ult_comp:
        compra = True
    else:        
        compra = False
        

    return compra
    

def busca_valles(conf_client,market,params,cartera):
    
    if market == 'EUR-BTC':
        market = 'BTC-EUR';    
    
    cartera_posible = dict()
    
    #Se calcula el precio actual
    newData = conf_client.client.get_product_ticker(product_id=market)
    media_ult=float(newData['price'])
    
    #Contra el precio de hace 15 minutos
    media_penult = ut.calcula_medias_abs(conf_client,market,15)
    
    media_dia = ut.calcula_medias_abs(conf_client,market,360)
    
    diff_current_ult = 0
    porcent = 0
    
    try:
        porcent = (media_ult - media_penult) / media_penult * 100
        
        #Calculo el precio maximo de las ultimas 5 horas
        precio_max = ut.calcula_precio_max(conf_client,market,86400)
    except:
        print(media_ult)
        print(media_penult)        
    
    try:
        newData = conf_client.client.get_product_ticker(product_id=market)
        currentPrice=float(newData['price'])
        
        media_ult_eje = ut.calcula_medias_abs(conf_client,market,3)
        #Esto dice si esta subiendo respecto 15m atras
        diff_current_ult = (currentPrice - media_ult_eje) / media_ult_eje * 100        
    except:
       print(newData)
    
    

    log.completo('Busca valle en market: '+ market)

    #Si ha pasado mas de dos horas reinicio el precio mercado, ya que es posible que no baje
    startdate = params.precio_ultima_venta_fecha.strftime("%Y-%m-%dT%H:%M") 
    enddate = datetime.now(tz=timezone.utc).strftime("%Y-%m-%dT%H:%M")
    diff_hour = (datetime.strptime(enddate, "%Y-%m-%dT%H:%M") - datetime.strptime(startdate, "%Y-%m-%dT%H:%M"))
    horas_diff = diff_hour.total_seconds() / 3600
        
    
    if ut.mercado_a_la_baja(conf_client,cartera) or currentPrice >= precio_max * 0.97:
        log.completo('Se usa perfil bajo ya que el mercado esta a la baja')
        if parametros_compra_prefil_bajo(porcent,diff_current_ult,currentPrice,params,horas_diff,precio_max):
            log.completo('Si considera valle para market: '+ market)
            cartera_posible [market] = (media_dia - currentPrice) / currentPrice * 100
    else:
        log.completo('Se usa perfil alto ya que el mercado esta al alta')
        if parametros_compra_prefil_alto(porcent,diff_current_ult,currentPrice,params,horas_diff,precio_max):
            log.completo('Si considera valle para market: '+ market)
            cartera_posible [market] = (media_dia - currentPrice) / currentPrice * 100
        
            
        
    return cartera_posible 
        
    
#####################################    
    #Inicializo el dict
def calcula_moneda_inver(conf_client,cartera,params):

 
    monedas={'BTC'}

    market_destino = dict()
    
    contador = 1
    market_final = None
    bool_caso_tercera_moneda = False
    
    for x in monedas:
        mercado_formado = ut.compone_mercado(x,cartera.moneda_actual)
        market_destino_recibido=busca_valles(conf_client, mercado_formado,params,cartera)
        
        if mercado_formado == 'BTC-EUR' and market_destino_recibido and cartera.moneda_actual not in ('BTC','EUR'):
            market_final = cartera.moneda_actual + '-BTC'
            bool_caso_tercera_moneda = True
        
        if market_destino_recibido:
            market_destino.update(market_destino_recibido)
        if contador == 2:
            time.sleep(2)
            contador=0
        
        contador = contador + 1
    
        
    #ordeno la market_destino y selecciono el ultimo elemento    
    if market_destino:
        market_destino=sorted(market_destino.items(),key=lambda x:x[1])
        if bool_caso_tercera_moneda == False:
            market_final = market_destino[-1][0]
               
    return market_final
    