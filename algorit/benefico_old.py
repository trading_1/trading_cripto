import algorit.calcula_inversion as ci
import utils_trading.log_proyect as log
import utils_trading.utils as ut

#Algoritmo que calcula si debemos donar dinero
def estabilidad_market(conf_client,cartera,params):

    newData = conf_client.client.get_product_ticker(product_id=cartera.market_actual)
    media_ult=float(newData['price'])
    
    media_penult = ut.calcula_medias_abs(conf_client,cartera.market_actual,5)
        
    porcent_func_estabilidad_market = (media_ult - media_penult) / media_penult * 100
    
    return porcent_func_estabilidad_market

def calcula_ganancia_minima(conf_client,cartera,params):
    
    bool_recupera_ganacia = False
    
    if params.precio_ultima_compra != 0 and cartera.moneda_actual == 'BTC':
        result = params.precio_ultima_compra * 1.008
    elif params.precio_ultima_compra != 0 and cartera.moneda_actual != 'BTC':
        result = params.precio_ultima_compra * 1.008
    else:
        bool_recupera_ganacia = True

    if bool_recupera_ganacia == False:
        if cartera.conversion_euros_actual >= result:
            bool_recupera_ganacia = True
    
    return bool_recupera_ganacia

def market_en_descenso(cartera,porcent):
    
    bool_moneda_descenso = False
    
    if cartera.moneda_actual == 'BTC':
        if porcent <= -0.05:
            bool_moneda_descenso = True
    
    elif cartera.moneda_actual != 'EUR' and cartera.moneda_actual != 'BTC':
         if porcent <= -0.05:
            bool_moneda_descenso = True
    
    return bool_moneda_descenso

def supera_min_perdida(cartera,params):
    
    bool_func_supera_min_perdida = False

    if cartera.moneda_actual != 'EUR':
        if cartera.conversion_euros_actual < params.min_precio_perdida:
            bool_func_supera_min_perdida = True
    
    return bool_func_supera_min_perdida
    

def accion_realizar(conf_client,cartera,params):

    accion                        = 'espera'
    porcent_estabilidad_market    = estabilidad_market(conf_client,cartera,params)
    bool_market_descenso          = market_en_descenso(cartera,porcent_estabilidad_market)
    
    log.completo('Porcentaje de estabilidad del market {} es {}'.format(cartera.market_actual,porcent_estabilidad_market))
    log.completo('El market actual {} se considera en descenso: {}'.format(cartera.market_actual,bool_market_descenso))

    if cartera.moneda_actual == 'EUR':
        #Si estamos en Euros

        market_dest = ci.calcula_moneda_inver(conf_client,cartera,params)
        if market_dest == 'BTC-EUR':
            cartera.market_actual=market_dest
            if ut.mercado_a_la_baja(conf_client,cartera) == False:
                accion = 'compra_desde_eur'
            else:
                accion = 'mercado_a_la_baja'
                
    else:
        #Si NO estamos en Euros
        
        recupera_inver                  = calcula_ganancia_minima(conf_client,cartera,params)
        bool_supera_min_perdida         = supera_min_perdida(cartera,params)
        

        log.completo('Se ha recuperado la ultima inversion: {}'.format(recupera_inver))    
        log.completo('Se ha superado el precio minimo de perdida: {}'.format(bool_supera_min_perdida))

        if accion == 'espera':

            if bool_market_descenso and recupera_inver:
                market_dest = ci.calcula_moneda_inver(conf_client,cartera,params)
                if market_dest:
                    if ut.mercado_a_la_baja(conf_client,cartera) == False and cartera.moneda_actual=='BTC':
                        cartera.market_actual=market_dest            
                        accion = 'cambia_moneda'
                    else:
                        #Si no hay mercado y esta bajando, se vende a Euros
                        cartera.market_actual=cartera.moneda_actual+'-EUR'
                        accion = 'venta_a_euros'
                else:
                        #Si no hay mercado y esta bajando, se vende a Euros
                    cartera.market_actual=cartera.moneda_actual+'-EUR'
                    accion = 'venta_a_euros'
            

        if accion == 'espera':

            if  bool_supera_min_perdida:
                market_dest = ci.calcula_moneda_inver(conf_client,cartera,params)
                
                if market_dest:
                    if ut.mercado_a_la_baja(conf_client,cartera) == False and cartera.moneda_actual=='BTC':
                        cartera.market_actual=market_dest
                        accion = 'cambia_moneda'
                    else:
                        #Si no hay mercado y esta bajando, se vende a Euros
                        cartera.market_actual=cartera.moneda_actual+'-EUR'
                        accion = 'venta_a_euros'    
        
                else:
                    #Si no hay mercado y esta bajando, se vende a Euros
                    cartera.market_actual=cartera.moneda_actual+'-EUR'
                    accion = 'venta_a_euros'    
        
    return accion
