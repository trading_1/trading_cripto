import pygame
import sys

# Inicializa Pygame.
pygame.init()

# Configura la pantalla.
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Pong")

# Configura los colores.
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Configura las palas.
paddle_a = pygame.Rect(300, 200, 10, 100)
paddle_b = pygame.Rect(770, 200, 10, 100)

# Configura la bola.
ball = pygame.Rect(400, 300, 10, 10)

# Configura la velocidad de la bola.
ball_speed_x = 7
ball_speed_y = 7

# Configura la puntuación.
score_a = 0
score_b = 0

# Bucle principal del juego.
while True:
    # Maneja los eventos.
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                paddle_a.y -= 10
            elif event.key == pygame.K_s:
                paddle_a.y += 10
            elif event.key == pygame.K_UP:
                paddle_b.y -= 10
            elif event.key == pygame.K_DOWN:
                paddle_b.y += 10

    # Mueve la bola.
    ball.x += ball_speed_x
    ball.y += ball_speed_y

    # Rebota la bola en los bordes de la pantalla.
    if ball.x < 0 or ball.x > 800:
        ball_speed_x = -ball_speed_x
    if ball.y < 0 or ball.y > 600:
        ball_speed_y = -ball_speed_y

    # Rebota la bola en las palas.
    if ball.colliderect(paddle_a):
        ball_speed_x = -ball_speed_x
    if ball.colliderect(paddle_b):
        ball_speed_x = -ball_speed_x

    # Aumenta la puntuación si la bola pasa por una de las palas.
    if ball.x < 0:
        score_b += 1
    if ball.x > 800:
        score_a += 1

    # Dibuja la pantalla.
    screen.fill(BLACK)
    pygame.draw.rect(screen, WHITE, paddle_a)
    pygame.draw.rect(screen, WHITE, paddle_b)
    pygame.draw.rect(screen, WHITE, ball)

    # Muestra la puntuación.
    font = pygame.font.SysFont("Arial", 30)
    text_a = font.render(str(score_a), True, WHITE)
    text_b = font.render(str(score_b), True, WHITE)
    screen.blit(text_a, (350, 10))
    screen.blit(text_b, (450, 10))

    # Actualiza la pantalla.
    pygame.display.update()

    # Limita la velocidad del juego a 60 fotogramas por segundo.
    pygame.time.Clock().tick(60)