# -*- coding: utf-8 -*-
"""
Utilidades para operaciones de trading.

@author: depabls
"""

import utils_trading.log_proyect as logger
import gcp_utils.bq as bigquery
from datetime import datetime, timedelta, timezone
import yfinance as yf
import pandas as pd


def round_down(value, decimals):
    """Redondea un valor hacia abajo con la precisión especificada."""
    factor = 10 ** decimals
    return (value // factor) * factor


def calcular_precision(conf_client, cartera):
    """Calcula la precisión decimal permitida para la moneda actual."""
    cuentas = conf_client.client.get_accounts()
    precision = 0

    for cuenta in cuentas.data:
        if cuenta['balance']['currency'] == cartera.moneda_actual:
            try:
                _, decimales = cuenta['balance']['amount'].split('.')
                precision = len(decimales)
            except ValueError:
                precision = 0  # No hay decimales
            break

    logger.completo(f"Precisión para {cartera.moneda_actual}: {precision}")
    return precision


def calcular_tendencia(valor_anterior, valor_actual):
    """Calcula la variación porcentual entre dos valores."""
    if valor_anterior == 0:
        return 0  # Evitar divisiones por cero
    return ((valor_actual - valor_anterior) / valor_anterior) * 100


def retorna_valor_precision(valor, precision):
    """Ajusta un valor a la precisión dada."""
    # Convertir el valor a cadena para poder verificar la parte decimal
    valor_str = str(valor)

    # Verificar si el valor tiene un punto decimal
    if '.' in valor_str:
        entero, decimales = valor_str.split('.')
        # Limitar la cantidad de decimales según la precisión especificada
        return f"{entero}.{decimales[:precision]}"

    # Si no tiene punto decimal, devolver el valor tal cual
    return valor_str


def quita_riesgo(valor):
    """Reduce un porcentaje del valor para minimizar riesgos."""
    return valor * 0.90  # Reduce el 1.5% del valor


def min_perdida(cartera, precio_ultima_compra=0):
    """Calcula el saldo mínimo de venta basado en la moneda actual."""
    if cartera.moneda_actual == 'EUR':
        return quita_riesgo(cartera.cartera_euros)
    return quita_riesgo(precio_ultima_compra)


def compra_desde_eur(conf_client, cartera, params):
    """Ejecuta una compra de BTC usando euros."""
    precision = calcular_precision(conf_client, cartera)
    cantidad = retorna_valor_precision(cartera.invertido, precision)

    logger.completo(f"Comprando BTC con EUR en {cartera.market_actual}, cantidad: {cantidad}")
    conf_client.client_order.create_buy_market_order(
        client_order_id=str(params.id_ejecucion_actual),
        product_id='BTC-EUR',
        quote_size=cantidad
    )
    params.precio_ultima_compra = cartera.conversion_euros_actual


def venta_a_euros(conf_client, cartera, params):
    """Vende BTC y lo convierte a euros."""
    precision = calcular_precision(conf_client, cartera)
    cantidad = retorna_valor_precision(cartera.invertido, precision)

    logger.completo(f"Vendiendo {cartera.moneda_actual} a EUR, cantidad: {cantidad}")
    conf_client.client_order.create_sell_market_order(
        client_order_id=str(params.id_ejecucion_actual),
        product_id='BTC-EUR',
        base_size=cantidad
    )
    params.precio_ultima_compra = cartera.conversion_euros_actual


def actualiza_control_proces(conf_client, cartera, params, accion):
    """Actualiza la tabla de control con datos de la ejecución."""
    fila = [{
        'id_cuenta': 1,
        'id_ejecucion': params.id_ejecucion_actual,
        'moneda_actual': cartera.moneda_actual,
        'market_actual': cartera.market_actual,
        'invertido': cartera.invertido,
        'precio_mercado': cartera.precio_mercado,
        'cartera_euros': cartera.cartera_euros,
        'conversion_euros_actual': cartera.conversion_euros_actual,
        'precio_minimo_venta': params.min_precio_perdida,
        'precio_ultima_compra': params.precio_ultima_compra,
        'accion': accion,
        'fec_eje': datetime.now().strftime("%Y%m%d_%H%M"),
        'valoracion_inversion': params.valoracion_inversion
    }]

    bigquery.insert_row(conf_client, fila)


def calcular_rsi(precios, periodo=14):
    """Calcula el RSI a partir de una lista de precios de cierre sin TA-Lib."""
    # Calculamos las diferencias diarias de los precios
    diferencia = precios.diff()

    # Calculamos las ganancias y las pérdidas
    ganancia = diferencia.where(diferencia > 0, 0)
    perdida = -diferencia.where(diferencia < 0, 0)

    # Calculamos las medias de las ganancias y pérdidas
    ganancia_media = ganancia.rolling(window=periodo).mean()
    perdida_media = perdida.rolling(window=periodo).mean()

    # Calculamos el RSI
    rs = ganancia_media / perdida_media
    rsi = 100 - (100 / (1 + rs))

    return rsi.iloc[-1]

def calcula_max_min_abs(calc, tiempo_inicio_minutos, delay=-5):
    """Calcula valores máximos, mínimos o tendencias basados en datos históricos."""
    btc_ticker = yf.Ticker("BTC-EUR")
    btc_data = btc_ticker.history(period="5d", interval="1m")

    tiempo_final_minutos =  tiempo_inicio_minutos + delay
    startdate = (datetime.now(tz=timezone.utc) - timedelta(minutes=tiempo_inicio_minutos)).strftime("%Y-%m-%dT%H:%M")
    enddate = (datetime.now(tz=timezone.utc) - timedelta(minutes=tiempo_final_minutos)).strftime("%Y-%m-%dT%H:%M")

    datos_historicos = btc_data.loc[startdate:enddate]
    datos_historicos.sort_index(ascending=True, inplace=True)

    if calc == 'max':
        return datos_historicos['Close'].max()
    elif calc == 'min':
        return datos_historicos['Close'].min()
    elif calc == 'mean':
        return datos_historicos['Close'].mean()
    elif calc == 'ten':
        return calcular_tendencia(datos_historicos['Close'].iloc[0], datos_historicos['Close'].iloc[-1])
    elif calc == 'rsi':
        return calcular_rsi(datos_historicos['Close'])
    return 0


def compone_mercado(moneda_destino, moneda_actual):
    """Compone el mercado en función de las monedas involucradas."""
    if moneda_actual in ['EUR', 'BTC']:
        return f"{moneda_destino}-{moneda_actual}"
    return 'BTC-EUR'
