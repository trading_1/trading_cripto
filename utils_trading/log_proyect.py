# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 13:25:25 2019

@author: depabls
"""
import utils_trading.send_slack as sl
import utils_trading.telegram as tl
from datetime import datetime

def completo(message):
    # Obtener la hora actual
    hora_actual = datetime.now().time()

    hora_inicio = datetime.strptime('23:00:00', '%H:%M:%S').time()
    hora_fin = datetime.strptime('06:00:00', '%H:%M:%S').time()

    # Verificar si la hora actual está dentro del rango
    if not(hora_inicio <= hora_actual <= hora_fin):
        slack(message)
        telegram(message)
        

    consola(message)
    
def slack(message):
    sl.charla_slack(parseaLog(message))

def telegram(message):
    tl.charla_telegram(parseaLog(message))


def consola(message):
    print(parseaLog(message))

def parseaLog(message):
    return datetime.now().strftime("%H:%M") +' '+ message;
