#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Clase que devuelve un objeto con el estado del monedero y la inversión actual.

@author: Sergio
"""


class GetWallet:
    def __init__(self, conf_client):
        """Inicializa el monedero con los balances y cálculos necesarios."""
        # Inicializa el dict de la cartera con las cuentas disponibles
        self.cartera = self.obtener_cartera(conf_client)

        # Determina el mercado y moneda actual
        self.market_actual = 'EUR-BTC'
        self.moneda_actual = self.cartera[-1][0]  # Moneda con mayor balance

        # Obtiene el balance específico y los euros disponibles
        self.invertido = self.get_specific_account(conf_client, self.moneda_actual)
        self.cartera_euros = self.get_euros(conf_client)

        # Obtiene el precio actual de mercado
        self.precio_mercado = self.obtener_precio_mercado(conf_client, self.market_actual)

        # Realiza la conversión si la moneda actual no es EUR
        if self.moneda_actual != 'EUR':
            self.conversion_euros_actual = self.convertir_market_a_euros(self.invertido, self.precio_mercado)
            self.cartera_euros = self.conversion_euros_actual
        else:
            self.conversion_euros_actual = float(self.cartera_euros)

    def obtener_cartera(self, conf_client):
        """Obtiene y organiza el monedero eliminando monedas no deseadas."""
        cuentas = conf_client.client.get_accounts()
        cartera = {
            account['currency']['code']: float(account['balance']['amount'])
            for account in cuentas.data
        }

        # Elimina monedas específicas
        cartera.pop('OMG', None)
        cartera.pop('USDC', None)

        # Elimina euros si el balance es demasiado bajo
        if cartera.get('EUR', 0) <= 0.02:
            cartera.pop('EUR', None)

        # Ordena la cartera por balance en orden ascendente
        return sorted(cartera.items(), key=lambda x: x[1])

    def get_specific_account(self, conf_client, moneda_actual):
        """Obtiene el balance específico para una moneda."""
        cuentas = conf_client.client.get_accounts()
        for account in cuentas.data:
            if account['currency']['code'] == moneda_actual:
                return float(account['balance']['amount'])
        return 0

    def get_euros(self, conf_client):
        """Obtiene el balance en euros."""
        cuentas = conf_client.client.get_accounts()
        for account in cuentas.data:
            if account['currency']['code'] == 'EUR':
                return round(float(account['balance']['amount']), 2)
        return 0

    def obtener_precio_mercado(self, conf_client, market_actual):
        """Obtiene el precio actual del mercado para el par especificado."""
        precio_data = conf_client.client.get_spot_price(currency_pair='BTC-EUR')
        return float(precio_data['amount'])

    def convertir_market_a_euros(self, invertido, precio_mercado):
        """Convierte el balance de la moneda actual a euros."""
        try:
            return float(invertido) * float(precio_mercado)
        except Exception as e:
            print(f"Error en la conversión: {e}")
            return 0
