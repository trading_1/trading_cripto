#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 15:06:21 2021

@author: sergio
"""

from slack import WebClient
from slack.errors import SlackApiError

#slack_token = 'xoxb-1723982992518-1724509280950-NwGqHEUjXgkzox0DZYKbWxKN'
slack_token = 'aa'
client = WebClient(token=slack_token)

def charla_slack(message):
    response = None
    try:
      response = client.chat_postMessage(
        channel="#modelo_simple_v2",
        text=message
      )
    except SlackApiError as e:
      # You will get a SlackApiError if "ok" is False
      assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
    return response