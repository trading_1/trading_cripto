#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Clase que devuelve un objeto con los parámetros del sistema.

@author: Sergio
"""
import gcp_utils.bq as bq
import utils_trading.utils as ut


class Parametros:
    def __init__(self, conf_client, cartera):
        """Inicializa los parámetros desde BigQuery o valores calculados."""
        self.conf_client = conf_client
        self.cartera = cartera

        num_reg = self.obtener_numero_registros()

        if num_reg == 0:
            self._inicializar_nuevos_parametros()
        else:
            self._cargar_parametros_existentes()

        # Valores por defecto en caso de ausencia
        if self.precio_ultima_compra is None:
            self.precio_ultima_compra = self.min_precio_perdida * (conf_client.riesgo_precio_minimo + 1)

        if self.valoracion_inversion is None:
            self.valoracion_inversion = 'Ganancia'

    def obtener_numero_registros(self):
        """Consulta el número de registros en la tabla."""
        sql = f'''
        SELECT COUNT(1) as num_reg 
        FROM `{self.conf_client.project}.{self.conf_client.bq_dataset_name}.{self.conf_client.table_name}`;
        '''
        resultado = bq.execute_sql_scripts(self.conf_client, sql)
        for row in resultado:
            return row.num_reg
        return 0

    def _inicializar_nuevos_parametros(self):
        """Inicializa los parámetros en caso de ser el primer registro."""
        self.min_precio_perdida = ut.min_perdida(self.conf_client, self.cartera)
        self.id_ejecucion_actual = 1
        self.precio_ultima_compra = self.min_precio_perdida * (self.conf_client.riesgo_precio_minimo + 1)

        ticker_data = self.conf_client.client_pro.get_product_ticker(product_id='BTC-EUR')
        self.precio_ultima_venta_mercado = float(ticker_data['price'])
        self.valoracion_inversion = None

    def _cargar_parametros_existentes(self):
        """Carga los parámetros de un registro previo."""
        self.id_ejecucion_ant = self.get_parametro_max_id_ejecucion()
        self.id_ejecucion_actual = self.id_ejecucion_ant + 1

        self.precio_ultima_compra = self.get_parametro_precio_ultima_compra(self.id_ejecucion_ant)
        self.min_precio_perdida = ut.min_perdida(self.cartera, self.precio_ultima_compra)
        self.precio_ultima_venta_mercado, self.precio_ultima_venta_fecha = self.get_parametro_ultima_venta_mercado()
        self.valoracion_inversion = self.get_parametro_valoracion_inversion(self.id_ejecucion_actual)

    def get_parametro_max_id_ejecucion(self):
        """Obtiene el ID de ejecución más alto en la tabla."""
        sql = f'''
        SELECT MAX(id_ejecucion) as num_reg 
        FROM `{self.conf_client.project}.{self.conf_client.bq_dataset_name}.{self.conf_client.table_name}`;
        '''
        resultado = bq.execute_sql_scripts(self.conf_client, sql)
        for row in resultado:
            return row.num_reg

    def get_parametro_precio_ultima_compra(self, id_ejecucion):
        """Obtiene el precio de la última compra dado un ID de ejecución."""
        sql = f'''
        SELECT precio_ultima_compra as num_reg 
        FROM `{self.conf_client.project}.{self.conf_client.bq_dataset_name}.{self.conf_client.table_name}` 
        WHERE id_ejecucion = {id_ejecucion};
        '''
        resultado = bq.execute_sql_scripts(self.conf_client, sql)
        for row in resultado:
            return row.num_reg

    def get_parametro_ultima_venta_mercado(self):
        """Obtiene el precio y la fecha de la última venta en el mercado."""
        sql = f'''
        SELECT precio_mercado, PARSE_DATETIME("%Y%m%d_%H%M", fec_eje) as fec_eje
        FROM `{self.conf_client.project}.{self.conf_client.bq_dataset_name}.{self.conf_client.table_name}` 
        WHERE id_ejecucion IN (
            SELECT MAX(id_ejecucion) 
            FROM `{self.conf_client.project}.{self.conf_client.bq_dataset_name}.{self.conf_client.table_name}` 
            WHERE moneda_actual = "BTC"
        );
        '''
        resultado = bq.execute_sql_scripts(self.conf_client, sql)
        for row in resultado:
            return row.precio_mercado, row.fec_eje

    def get_parametro_valoracion_inversion(self, id_ejecucion):
        """Obtiene la valoración de inversión más alta asociada al ID de ejecución."""
        sql = f'''
        SELECT MAX(valoracion_inversion) as num_reg 
        FROM `{self.conf_client.project}.{self.conf_client.bq_dataset_name}.{self.conf_client.table_name}` 
        WHERE id_ejecucion = {id_ejecucion};
        '''
        resultado = bq.execute_sql_scripts(self.conf_client, sql)
        for row in resultado:
            return row.num_reg
