# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 13:25:25 2019

@author: depabls
"""

#from flask.config import Config
from coinbase.wallet.client import Client
from coinbaseadvanced.client import CoinbaseAdvancedTradeAPIClient

import configparser


class config_project:   
    def __init__(self,fichero):

        self.configuracion = configparser.ConfigParser()
        self.configuracion.read(fichero)

        self.apiKey = self.configuracion ['General']['apiKey']
        self.apiSecret = self.configuracion ['General']['apiSecret']

        self.passphrase = self.configuracion ['General']['passphrase']

        self.period =self.configuracion.getint('General', 'period')
        self.comision =self.configuracion.getfloat('General', 'comision')
        self.riesgo_precio_minimo =self.configuracion.getfloat('General', 'riesgo_precio_minimo')
        
        self.private_key =self.configuracion['General']['private_key']
        self.project =self.configuracion['General']['project']
        self.bq_dataset_name =self.configuracion['General']['bq_dataset_name']
        self.table_name =self.configuracion['General']['table_name']

        self.client = Client(self.apiKey, self.apiSecret)
    
        self.client_order = CoinbaseAdvancedTradeAPIClient(self.apiKey, self.apiSecret)