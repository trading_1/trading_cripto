import requests

TOKEN = "6706128240:AAFi7JosBYL-RWQe-5p1L0IYf8RcYFKVMm0"
CHAT_ID = "6322051419"

def charla_telegram(message):
    base_url = f'https://api.telegram.org/bot{TOKEN}/'
    # Construye el cuerpo de la solicitud POST

    data = {
        'chat_id': CHAT_ID,
        'text': message
    }

    # Construye la URL para enviar el mensaje
    send_message_url = f'{base_url}sendMessage'

    # Envía la solicitud POST
    try:
        response = requests.post(send_message_url, data=data)
    except requests.exceptions.RequestException as e:
        print(f"Error en la solicitud: {e}")
