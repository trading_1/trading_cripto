#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Clase para analizar y devolver el estado del mercado.

@author: Sergio
"""
import utils_trading.utils as ut
import utils_trading.log_proyect as log


class Market:
    def __init__(self, conf_client, cartera):
        """Inicializa el estado del mercado usando datos históricos."""
        self.conf_client = conf_client
        self.cartera = cartera

        self.precio_actual = self.obtener_precio_actual()
        self.tendencia_5m = self.calcular_tendencia(6)
        self.tendencia_60m = self.calcular_tendencia(60)
        self.tendencia_24h = self.calcular_tendencia(1440)
        self.precio_minimo_8h = self.calcular_precio('min', 480)
        self.precio_maximo_8h = self.calcular_precio('max', 480)
        self.precio_minimo_24h = self.calcular_precio('min', 1440)
        self.precio_maximo_24h = self.calcular_precio('max', 1440)
        self.precio_maximo_hist = self.calcular_precio('max', 16000)
        self.valor_rsi_1h = self.calcular_rsi(60)

        self.venta_precio_limite = False
        self.compra_precio_limite = False
        self.moneda_actual = cartera.moneda_actual

    def analista_mercado(self):
        """Analiza el mercado y ajusta las señales según tendencias y precios."""
        log.completo(f"Precio mínimo 24h: {self.precio_minimo_24h}")
        log.completo(f"Precio máximo 24h: {self.precio_maximo_24h}")
        log.completo(f"Precio mínimo 8h: {self.precio_minimo_8h}")
        log.completo(f"Precio máximo 8h: {self.precio_maximo_8h}")
        log.completo(f"Tendencia 5m: {self.tendencia_5m}")
        log.completo(f"Precio de compra: {self.calcular_precio_compra(0.25)}")
        log.completo(f"Tendencia 60m: {self.tendencia_60m}")
        log.completo(f"Precio de compra tendencia >1 en 60m: {self.calcular_precio_compra(0.50)}")
        log.completo(f"Precio de venta: {self.precio_maximo_24h * 0.982}")
        log.completo(f"Precio hist range: {self.precio_maximo_hist * 0.982}")
        log.completo(f"RSI 1h: {self.valor_rsi_1h}")

        self.venta_precio_limite = self.moneda_actual == 'BTC' and self.precio_actual >= self.precio_maximo_8h * 0.982
        self.compra_precio_limite = self.moneda_actual != 'BTC' and self.precio_actual <= self.calcular_precio_compra(0.25)

        self.venta_por_tendencia_corta = self.tendencia_5m <= -0.025
        self.compra_por_tendencia_corta = not self.venta_por_tendencia_corta

        self.compra_rsi_1h = self.valor_rsi_1h < 30 # RSI < 30: señal de sobreventa, comprar
        self.venta_rsi_1h = self.valor_rsi_1h > 70 # RSI > 70: señal de sobrecompra, vender

        self.mercado_a_la_baja = self.tendencia_24h <= -0.06
        self.mercado_cerca_del_hist = self.precio_actual >= self.precio_maximo_hist * 0.982


        return self

    def obtener_precio_actual(self):
        """Obtiene el precio actual del mercado."""
        new_data = self.conf_client.client.get_spot_price(currency_pair='BTC-EUR')
        return float(new_data['amount'])

    def calcular_tendencia(self, minutos):
        """Calcula la tendencia de precios en los últimos minutos."""
        return ut.calcula_max_min_abs('ten', minutos, -minutos)

    def calcular_rsi(self, minutos):
        """Calcula la tendencia de rsi en los últimos minutos."""
        return ut.calcula_max_min_abs('rsi', minutos, -minutos)

    def calcular_precio(self, tipo, minutos):
        """Calcula el precio máximo, mínimo o promedio según un período."""
        return ut.calcula_max_min_abs( tipo, minutos, -minutos)

    def calcular_precio_compra(self, factor):
        """Calcula el precio ideal de compra según un factor."""
        rango = self.precio_maximo_8h - self.precio_minimo_8h
        return self.precio_minimo_8h + rango * factor
