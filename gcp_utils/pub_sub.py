# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 12:22:40 2020

@author: DEPABLS
"""
from google.cloud import pubsub_v1
from google.oauth2 import service_account

def publish_messages(configura):
    """Publishes multiple messages to a Pub/Sub topic."""
    # [START pubsub_quickstart_publisher]
    # [START pubsub_publish]

    credentials = service_account.Credentials.from_service_account_file(configura.private_key)

    publisher = pubsub_v1.PublisherClient(credentials=credentials)

    # The `topic_path` method creates a fully qualified identifier
    # in the form `projects/{project_id}/topics/{topic_name}`

    for topic_name,message in configura.message.items():

        topic_name = topic_name[:topic_name.find('sub')+3]    

        topic_path = publisher.topic_path(configura.project,topic_name)

        message=topic_name + '/' + message
  
        message = message.encode('utf-8')

        # When you publish a message, the client returns a future.
        future = publisher.publish(topic_path, data=message).result()
        print(future,"s")
    
        print("Published messages.")
        # [END pubsub_quickstart_publisher]
        # [END pubsub_publish]