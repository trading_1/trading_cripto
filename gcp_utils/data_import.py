from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

from googleapiclient.http import MediaFileUpload
from apiclient.errors import HttpError



# SET VARS



def get_service(api_name, api_version, scopes, key_file_location):
    """Get a service that communicates to a Google API.

    Args:
        api_name: The name of the api to connect to.
        api_version: The api version to connect to.
        scopes: A list auth scopes to authorize for the application.
        key_file_location: The path to a valid service account JSON key file.

    Returns:
        A service that is connected to the specified API.
    """

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            key_file_location, scopes=scopes)

    # Build the service object.
    service = build(api_name, api_version, credentials=credentials)

    return service

def uploadCSV(service,configura,file_id):    
    try:
        media = MediaFileUpload(file_id,
                          mimetype='application/octet-stream',
                          resumable=False)
                
        
        service.management().uploads().uploadData(
                    accountId=configura.account_id,
                    webPropertyId=configura.web_property_id,
                    customDataSourceId=configura.custom_data_source_id,
                    media_body=media
                ).execute()
                
    
    except TypeError as error:
      # Handle errors in constructing a query.
      print ('There was an error in constructing your query : %s' % error)
      
    except HttpError as error:
      # Handle API errors.
      print ('There was an API error : %s : %s' %
             (error.resp.status, error.resp.reason))  


def deleteCSV(service,configura):
    
    lista_items = []
    
    try:
        uploads = service.management().uploads().list(
                    accountId=configura.account_id,
                    webPropertyId=configura.web_property_id,
                    customDataSourceId=configura.custom_data_source_id
                 ).execute()    
              
    except TypeError as error:
      # Handle errors in constructing a query.
      print ('There was an error in constructing your query : %s' % error)
      
    except HttpError as error:
      # Handle API errors.
      print ('There was an API error : %s : %s' %
             (error.resp.status, error.resp.reason))  
    try:  
        lista=uploads['items']
    except:
        lista = None
        
    if lista:
        for l in range(5,len(lista)):
            lista_items.append(lista[l]['id'])        
    
    if lista_items:    
        try:    
            service.management().uploads().deleteUploadData(
                        accountId=configura.account_id,
                        webPropertyId=configura.web_property_id,
                        customDataSourceId=configura.custom_data_source_id,
                        body={
                                'customDataImportUids' : lista_items
                             }
                ).execute()  
            
        except TypeError as error:
          # Handle errors in constructing a query.
          print ('There was an error in constructing your query : %s' % error)
          
        except HttpError as error:
          # Handle API errors.
          print ('There was an API error : %s : %s' %
                 (error.resp.status, error.resp.reason))  
    else: print('No es necesario borrar')




