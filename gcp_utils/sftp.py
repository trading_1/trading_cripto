# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 13:25:25 2019

@author: depabls
"""
import paramiko

def ftp_send(configura,inFile,outFile):
    
    '''   
    sftpURL   =  '13.111.95.23'
    sftpUser  =  '7323142'
    sftpPass  =  'SFMC19Verti@'
    '''   
    ssh = paramiko.SSHClient()
    # automatically add keys without requiring human intervention
    ssh.set_missing_host_key_policy( paramiko.AutoAddPolicy() )
    
    ssh.connect(configura.sftpURL, username=configura.sftpUser, password=configura.sftpPass)
    
    ftp = ssh.open_sftp()
    
    ftp.put(inFile, outFile)