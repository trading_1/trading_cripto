# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 11:35:39 2020

@author: DEPABLS
"""


import json
import requests
import re
import datetime


def plexus_service_auth(ID,EndPoint):    
    
    '''
    uri_coti='http://wwwneurona-pre.verti.es.mapfre.net/plexus/rs/auth/1.0/cotizaciones/106740625'
    uri='http://wwwneurona-pre.verti.es.mapfre.net/plexus/rs/pub/1.0/tickets'
    params_user={'Plexus-User' : 'PLX_BI',
                     'Plexus-Password'  : 'PLX_BI',
                     }
        
    uri_coti='https://verti.es/plexus/rs/auth/1.0/cotizaciones/106740625'    
    '''
    api='1.0'

    uri_coti='http://www.verti.es/plexus/rs' + EndPoint.replace('{bid}',ID).replace('{id}',ID).replace('{version}',api)

    uri='https://www.verti.es/plexus/rs/pub/1.0/tickets'   

    
    params_user={'Plexus-User' : 'GCP_BI',
                 'Plexus-Password'  : 'V3gCPB1r',
                 }    
    
    response_ticket = requests.post(uri, headers=params_user)
    
    json_data = json.loads(response_ticket.text)
    
    
    params_ticket={'Plexus-Ticket' : json_data['plexusTicket']}    
    
    response = requests.get(uri_coti, headers=params_ticket)
            
    '''
    Se formatea el json
    '''
    json_data = json.loads(response.text)
    
    json_data['identificador']=ID
    
    salida_sn_format=json.dumps(json_data)
    
    fechas_list=re.findall(r"\b\d{1,2}[-/:]\d{1,2}[-/:]\d{4}\b",salida_sn_format)
    

    for fechas in fechas_list:
        salida_sn_format=salida_sn_format.replace(fechas,datetime.datetime.strptime(fechas, "%d/%m/%Y").strftime("%Y-%m-%d"))
    
    return salida_sn_format

