# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 13:25:25 2019

@author: depabls
funciones para carga en tablas bi_query
"""
import gcp_utils.bq  as bq
from etl_carga_bq import carga_tabla_logging as carga_tabla_logging

def flags_comp(configura):
    configura.config_flags('DEPENDENCIAS')
    
    process = ''
    
    if configura.flags:
            
        for e in configura.flags:
            sql = """SELECT count(1) as num_reg FROM `mapfre-verti-es.BI_ODS_VERTI.ODS_CM_F_CONTROL_PROCESOS_BI` 
                     WHERE DATE(_PARTITIONTIME) = current_date() and ESTADO_PROCESO = 'OK' and nom_proceso='"""+e+"""'"""
            
            result = bq.execute_sql_scripts(configura,sql)            
            
            for row in result:
                cnt = row.num_reg
            
            if cnt == 0:
                process = str(process) + e + ', '
        
        if process:
            error='Las siguientes dependencias no se han cumplido: ' + process[:-2]
            print (error)
            carga_tabla_logging.etl_carga_tabla_logging_error(configura,error)
            
            