# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 13:25:25 2019

@author: depabls
funciones para carga en tablas bi_query
"""

from google.cloud import bigquery
from google.api_core.exceptions import BadRequest



def load_table_from_uri_csv(configura,uri):
    
    client = bigquery.Client.from_service_account_json(configura.private_key)
    table_ref = client.dataset(configura.bq_dataset_name).table(configura.table_name)
    
    job_config = bigquery.LoadJobConfig()
    
    if configura.tinsert == 'truncate':        
        job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
    else: 
        job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND
        
    job_config.field_delimiter = configura.delimiter    
        
    job_config.skip_leading_rows = configura.skip_rows
    # The source format defaults to CSV, so the line below is optional.
    job_config.source_format = bigquery.SourceFormat.CSV
    
    job_config.max_bad_records = configura.brecord
    
    job_config.autodetect=True
    
    job_config.time_partitioning = bigquery.table.TimePartitioning()

    load_job = client.load_table_from_uri(
                                            uri, 
                                            table_ref, 
                                            job_config=job_config
                                          )  
    # API request   
    try:
        load_job.result()  # Waits for table load to complete.
    except BadRequest as e:
        for e in load_job.errors:
            error = 'ERROR: {}'.format(e['message'])
            print(error)
    except Exception as e:            
            error = e.message            
            print(error)
            
#obtener ultima fecha cargada en tabla
def delete_part(configura,date_id):
    client = bigquery.Client.from_service_account_json(configura.private_key)
    
    dataset_ref = client.dataset(configura.bq_dataset_name, project=configura.project)
    table_ref = dataset_ref.table(configura.table_name)
    
    part = view_partition_field (client,table_ref)
    
    if part:
        query = (
                'DELETE FROM `'+configura.project + '.' + configura.bq_dataset_name+ '.' + configura.table_name+'` WHERE DATE(' + part + ') ="' + date_id + '"')
    else:
        print ('Tabla no particionada o no particionado por DATE')
        return
    
    query_job = client.query(query,location='EU')  # API request - starts the query
    
     # API request   
    try:
        query_job.result()  # Waits for table load to complete.
    except BadRequest as e:
        for e in query_job.errors:
            error = 'ERROR: {}'.format(e['message'])
            print(error)
            
def view_partition_field (client,table_ref):
    
    try:
        table = client.get_table(table_ref)  # API Request
    except:
        return None
    
    if table.partitioning_type:
        if table.time_partitioning.field:
            part = table.time_partitioning.field
        else: 
            part = '_PARTITIONTIME'
    else:
        part = None

    return part

def execute_sql_scripts(configura,sql):
    
    client = bigquery.Client.from_service_account_json(configura.private_key)
    
    # Read query from GCS bucket.
    job = client.query(sql)        
                
    try:
        return job.result()
    except BadRequest as e:
        for e in job.errors:
            error = 'ERROR: {}'.format(e['message'])
            print(error)
    except Exception as e:            
            error = e.message            
            print(error)

def extrac_sql_scripts(configura,sql):
    
    client = bigquery.Client.from_service_account_json(configura.private_key)
    
    # Write query results to a new table
    job_config = bigquery.QueryJobConfig()
    table_ref = client.dataset(configura.bq_dataset_name).table(configura.table_name)
    job_config.destination = table_ref
    job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
        
    job = client.query(sql,location='EU',job_config=job_config)
                          
                
    try:
        job.result()
    except BadRequest as e:
        for e in job.errors:
            error = 'ERROR: {}'.format(e['message'])
            print(error)

    extrac_table_tmp(client,configura)


def extrac_table_tmp(client,configura):
    
    # Export table to GCS
    destination_uri = 'gs://bi-staging-mapfre-verti-es/Out/' + configura.table_name + '.csv'
    dataset_ref = client.dataset(configura.bq_dataset_name)
    table_ref = dataset_ref.table(configura.table_name)
    job_config = bigquery.job.ExtractJobConfig(print_header=False)


    extract_job = client.extract_table(
                    table_ref,
                    destination_uri,
                    job_config=job_config,
            location='EU')
    
          
    extract_job.result()  # Waits for job to complete
    '''
    except BadRequest as e:
        for e in extract_job.errors:
            print('ERROR: {}'.format(e['message']))
    
    '''
    print('Query results extracted to GCS: {}'.format(destination_uri))

    #client.delete_table(table_ref) #Deletes table in BQ

    #print('Table {} deleted'.format(table_id))

def query_bq(service_account_json,query):
    # [START bigquery_simple_app_client]
    client = bigquery.Client.from_service_account_json(service_account_json)
    # [END bigquery_simple_app_client]
    # [START bigquery_simple_app_query]
    query_job = client.query(query)

    results = query_job.result()  # Waits for job to complete.
    # [END bigquery_simple_app_query]

    # [START bigquery_simple_app_print]
    return results


def load_table_from_uri_json(service_account_json,uri,dataset_id,table_id,tipo_insert=None):

    client = bigquery.Client.from_service_account_json(service_account_json)
    table_ref = client.dataset(dataset_id).table(table_id)
    
    job_config = bigquery.LoadJobConfig()
    job_config.autodetect = True
    job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    
    if tipo_insert == 'truncate':        
        job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
    else: 
        job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND
    
    load_job = client.load_table_from_uri(
                                            uri, 
                                            table_ref, 
                                            job_config=job_config
    )  # API request
    try:
        load_job.result()  # Waits for table load to complete.
    except BadRequest as e:
        for e in load_job.errors:
            print('ERROR: {}'.format(e['message']))
            
  

def extract_schema_field (configura,field_id):
    client = bigquery.Client.from_service_account_json(configura.private_key)
    
    dataset_ref = client.dataset(configura.bq_dataset_name, project=configura.project)
    table_ref = dataset_ref.table(configura.table_name)
    table = client.get_table(table_ref)
    result = [schema.field_type for schema in table.schema if field_id in schema.name]       
    
    if result:
        return result[0]
    else:
        return None
 
def insert_row(configura,row):

    table_id=configura.project + '.' + configura.bq_dataset_name+ '.' + configura.table_name

    client = bigquery.Client.from_service_account_json(configura.private_key)

    rows_to_insert = row
  
    errors = client.insert_rows_json(table_id, rows_to_insert)  # Make an API request.

    if errors == []:

        print("New rows have been added.")

    else:

        print("Encountered errors while inserting rows: {}".format(errors))