# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 13:25:25 2019

@author: depabls
"""

from flask.config import Config

class config_project:
    '''Se crea el objeto de configuracion del proyecto'''
    def __init__(self,fichero):
        self.config = Config('.')
        self.config.from_pyfile(fichero, silent = False)
        
        '''Se obtiene el nombre del fichero'''        
        self.fichero = fichero[fichero.rfind('/') + 1:]
        
        self.project = self.config ['PROJECT']
        self.private_key = self.config ['PRIVATE_KEY']  
        self.origen = self.config ['ORIGEN'] 
        try:
            self.field_ng = self.config ['FIELD_NG'] 
        except:
            self.field_ng = None

    def config_bucket(self,bucket_name,dataset_name):
        self.bucket_name = self.config [bucket_name]
        self.destination_blob_name = self.config [dataset_name]
        
    def config_bq(self,dataset_name,table_name,skip_rows=None,delimiter=None,brecord=None,query=None,tinsert=None):
        self.bq_dataset_name = self.config [dataset_name]
        self.table_name = self.config [table_name]
        try:         
            self.skip_rows = self.config [skip_rows]  
        except:
            self.skip_rows = 1
            
        try:         
            self.delimiter = self.config [delimiter]  
        except:
            self.delimiter = ';'    
            
        try:         
            self.brecord = self.config [brecord]  
        except:
            self.brecord = 0    
        
        try:         
            self.tinsert = self.config [tinsert]  
        except:
            self.tinsert = None    
        
        try:         
            self.query = self.config [query]  
        except:
            self.query = None   
            
    def config_url (self,file_url,param_url=None):
        self.url = self.config [file_url]
        try:
            self.param_master = self.config [param_url]
        except:
            self.param_master = None
            
    def config_ga(self,account_id,web_property_id,custom_data_source_id):
        self.account_id = self.config [account_id]
        self.web_property_id = self.config [web_property_id]              
        self.custom_data_source_id = self.config [custom_data_source_id]          

    def config_pubsub(self,message = None):
        if message:            
            try:
                self.message = self.config [message]
            except:
                self.message = None

    def config_ftp(self,sftpURL,sftpUser,sftpPass,outFile):
        self.sftpURL = self.config [sftpURL]
        self.sftpUser = self.config [sftpUser]              
        self.sftpPass = self.config [sftpPass]
        self.outFile = self.config [outFile]

    def config_flags(self,flags = None):
        try:
            self.flags = self.config [flags]
        except:
            self.flags = None        