#!/usr/bin/python
#
# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Handles common tasks across all API samples."""

from googleapiclient import discovery
import httplib2
from oauth2client import client
from oauth2client.service_account import ServiceAccountCredentials
#from googleapiclient import http

def down_file_dm(file,profile_id,report_id):
  # Authenticate and construct service.
  service = setup(file)

  try:
    # 1. Find a file to download.
    report_file = find_file(service, profile_id, report_id)

    if report_file:
      request = direct_download_file(service, report_id, report_file)
      
    else:
      print ('No file found for profile ID %d and report ID %d.' % (profile_id,
                                                                   report_id))      
    return request

  except client.AccessTokenRefreshError:
    print('The credentials have been revoked or expired, please re-run the '
          'application to re-authorize')


def setup(file):

  API_NAME = 'dfareporting'
  API_VERSION = 'v3.3'

  OAUTH_SCOPES = ['https://www.googleapis.com/auth/dfareporting']

  credentials = ServiceAccountCredentials.from_json_keyfile_name(
      file,
      scopes=OAUTH_SCOPES)


  # Authorize HTTP object with the prepared credentials.
  http = credentials.authorize(http=httplib2.Http())

  # Construct and return a service object via the discovery service.
  return discovery.build(API_NAME, API_VERSION, http=http)


# Chunk size to use when downloading report files. Defaults to 32MB.

def find_file(service, profile_id, report_id):
  """Finds a report file to download."""
  target = None

  request = service.reports().files().list(
      profileId=profile_id, reportId=report_id)
  response = request.execute()
  '''
  while True:
    response = request.execute()
    

    for report_file in response['items']:
      if is_target_file(report_file):
        target = report_file
        break

    if not target and response['items'] and response['nextPageToken']:
      request = service.reports().files().list_next(request, response)
    else:
      break
  '''
  target = response['items'][0]['id']
  
  if target:
    print ('Found file %s with filename "%s".' % (response['items'][0]['id'],
                                                 response['items'][0]['fileName']))
    return target

  print ('Unable to find file for profile ID %d and report ID %d.' % (profile_id,
                                                                     report_id))
  return None


def is_target_file(report_file):
  # Provide custom validation logic here.
  # For example purposes, any available file is considered valid.
  return report_file['status'] == 'REPORT_AVAILABLE'



def direct_download_file(service, report_id, file_id):
  """Downloads a report file to disk."""
  # Retrieve the file metadata.
  report_file = service.files().get(
      reportId=report_id, fileId=file_id).execute()

  if report_file['status'] == 'REPORT_AVAILABLE':
    # Prepare a local file to download the report contents to.
    #out_file = io.FileIO(generate_file_name(report_file), mode='wb')

    # Create a get request.
    request = service.files().get_media(reportId=report_id, fileId=file_id)
    return request