# -*- coding: utf-8 -*-
"""
Archivo principal para ejecutar operaciones de trading en Binance.

@author: Sergio
"""

import utils_trading.utils as utils
import utils_trading.conf_proyect as config
import algorit.benefico_solo_BTC as algoritmo
import utils_trading.log_proyect as logger
import utils_trading.get_wallet as wallet
import utils_trading.parametros as parametros
import utils_trading.market as mercado


def http_trading(request):
    """Endpoint principal para ejecutar trading."""
    ejecutar_trading()
    return 'OK'

def registrar_estado_inicial(cartera, estado_mercado):
    """Registra en el log el estado inicial de la ejecución."""
    logger.completo(f"Moneda actual: {cartera.moneda_actual}")
    logger.completo(f"Euros disponibles: {cartera.cartera_euros}")
    logger.completo(f"Precio actual de BTC: {estado_mercado.precio_actual}")
    logger.completo(f"Tendencia del mercado (60m): {estado_mercado.tendencia_60m}")

    if cartera.moneda_actual != 'EUR':
        logger.completo(f"Invertido en {cartera.moneda_actual}: {cartera.invertido}")
        logger.completo(f"Precio de mercado: {cartera.precio_mercado}")
        logger.completo(f"Conversión a euros: {cartera.conversion_euros_actual}")



def ejecutar_trading():
    """Ejecuta el proceso completo de trading."""
    # Cargar configuración del proyecto
    conf_client = config.config_project(r'Instances/config.cfg')

    # Obtener estado inicial de la cartera
    cartera = wallet.GetWallet(conf_client)

    # Cargar parámetros previos y calcular nuevos
    params = parametros.Parametros(conf_client, cartera)

    # Obtener estado del mercado
    estado_mercado = mercado.Market(conf_client, cartera)

    # Log de inicio de ejecución
    logger.completo(f"### Inicio de ejecución: {params.id_ejecucion_actual} ###")
    registrar_estado_inicial(cartera, estado_mercado)

    # Determinar acción requerida
    accion = algoritmo.accion_realizar(conf_client, cartera, params, estado_mercado)
    logger.completo(f"Acción sugerida por el algoritmo: {accion}")

    # Ejecutar acción de compra o venta
    if accion == 'compra_desde_eur':
        utils.compra_desde_eur(conf_client, cartera, params)
    elif accion == 'venta_a_euros':
        utils.venta_a_euros(conf_client, cartera, params)

    # Actualizar tabla de control con datos finales
    utils.actualiza_control_proces(conf_client, cartera, params, accion)

    logger.completo(f"### Fin de ejecución: {params.id_ejecucion_actual} ###")


if __name__ == '__http_trading__':
    http_trading()
